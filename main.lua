half_life = 45
remaining = 0
run_length = 600
run_interval = 24
--dose_interval must be a multiple of run interval right now!
dose_interval = 24
dose_amount = 25

for time = 0, run_length, run_interval do
	if time % dose_interval == 0 then dose = dose_amount else dose = 0 end
	remaining = dose + remaining*(1/2)^(run_interval/half_life)
	print(remaining)
end